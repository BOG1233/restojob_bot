import re


class TextEditor:

    def __init__(self):
        self.supported_tags = {'a': '<a href="{}">{}</a>', 'b': '<b>{}</b>',
                               'strong': '<strong>{}</strong>',
                               'i': '<i>{}</i>', 'em': '<em>{}</em>',
                               'code': '<code>{}</code>', 'pre': '<pre>{}<pre>'}
        self.markdown_methods = {'bold': '*{}*', 'italic': '_{}_',
                                 'url': '[{}]({})', 'code': "'{}'",
                                 'pre': """'''{}'''"""}

    def html_editor(self, text=None, returned_img_src=False, file_name=None):
        if file_name is not None:
            text = open(file_name, 'r').read()
        if returned_img_src:
            text, img_src = self.delete_img_tags(text, True)
        tags_re = r'<.*?>'
        tags = re.findall(tags_re, text)
        for tag in tags:
            tag_name = self.find_tag_name(tag)
            # print(tag_name)
            if tag_name == 'br':
                text = re.sub(tag, '\n', text)
            elif tag_name == 'p':
                text = re.sub(tag, '\n\n', text)
            elif tag_name == 'a':
                new_link = self.do_link_teg(tag)
                text = re.sub(tag, new_link, text, count=1)
            elif re.search(r'h\d', tag_name):
                re.sub(tag, '', text)
                if '/' in tag:
                    text = re.sub(tag, '</b>', text)
                else:
                    text = re.sub(tag, '<b>', text)
            elif tag_name == 'html' or tag_name == 'body':
                text = re.sub(tag, '', text)
            else:
                text = re.sub(tag, '<Unsupported>', text)
        unsupported_re_full = r'<Unsupported>.*?<Unsupported>'
        unsupported_re = r'<Unsupported.*?>'
        text = re.sub(unsupported_re_full, '', text)
        text = re.sub(unsupported_re, '', text)
        if returned_img_src:
            return text, img_src
        return text

    def html_to_markdown(self, text):
        bold_re = r'(<b.*>.*?</b?>)|(<strong.*>.*</strong?>)'
        tags_re = r'<.*?>'
        italic_re = r'(<i.*>.*?</i?>)|(<em.*>.*</em?>)'
        code_re = r'(<code.*>.*?</code?>)|(<code.*>.*</code?>)'
        tags = re.findall(tags_re, text)
        for tag in tags:
            tag_name = self.find_tag_name(tag)
            if tag_name in self.supported_tags:
                continue
            else:
                text = re.sub(tag, '', text)
        return text

    @staticmethod
    def delete_img_tags(data, returned_src=False):
        img_re = r'<img.*>'
        img_tag = re.search(img_re, data)
        if img_tag:
            if returned_src:
                src = []
                src_re = r"""src=('|")(.*?)('|")"""
                text = img_tag.group()
                result = re.findall(src_re, text)
                for elm in result:
                    src.append(elm[1])
                data = re.sub(img_re, '', data, count=1)
                return data, src

            data = re.sub(img_re, '', data, count=1)
            return data
        else:
            if returned_src:
                return data, []
            return data

    @staticmethod
    def do_link_teg(text):
        if 'href' in text:
            href_re = r"""href=('|")(.*?)('|")"""
            href = re.search(href_re, text).group()
            text = re.sub(text, f'<a {href}>', text, count=1)
        return text

    @staticmethod
    def find_tag_name(text):
        name = ''
        breaking_char = [' ', '>']
        for l in text:
            if l in ['/', '<']:
                continue
            if l not in breaking_char:
                name += l
            else:
                break
        return name


if __name__ == '__main__':
    t = TextEditor()
    g, c = t.html_editor(text="""<a href='1'>Text</a>""", returned_img_src=True)
    print(g, c)

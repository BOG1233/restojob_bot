from flask import Flask, jsonify, request, abort
from src.DataBase import *
from src.token_gen import check_token
from src.bot import RequestBot
from requests import get
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from rj_bot.TelegramTextEditor import TextEditor

sentry_sdk.init(
    dsn="https://c741bd1c15e547639d4cd1aaffadc7cb@sentry.io/1533482",
    integrations=[FlaskIntegration()]
)

app = Flask(__name__)

# Database
session = Session()


def find_category(specialisation_id):
    res = get('https://restojob.ru/api/categories/')
    res = res.json()
    for spec_set in res['results']:
        spec_set_id = spec_set['id']
        for specialisation in spec_set['specialization_set']:
            if specialisation_id == specialisation:
                return spec_set_id
    return None


# Routes
@app.route('/api/v1/post_vacancy/<token>', methods=['POST'])
def post_vacancy(token):
    """:param token - auth token in url

       :example {'type': 'str',
                 'data': str',
                 'url': str,
                 'category': int,
                 'id': int,
                 'phone': str}"""
    if not check_token(session, token):
        return jsonify({'code': 301,
                        'description': 'invalid token'})
    else:
        if not request.json:
            return jsonify({'code': 452,
                            'description': 'not json request'})
        else:
            body = request.json
            if body.get('type') is not None:
                if body.get('data') is not None:
                    if body.get('category') is not None:
                        if body.get('url') is not None:
                            if body.get('id') is not None:
                                msg_type = body.get('type')
                                if msg_type == 'html':
                                    bot = RequestBot(session)
                                    category = body.get('category')
                                    data = body.get('data')
                                    editor = TextEditor()
                                    data = editor.html_editor(data)
                                    if len(data) < 1:
                                        return jsonify({'code': 400,
                                                        'description': 'text is empty'})

                                    errors = bot.post_vacancy(data, body.get('category'), vacancy_url=body.get('url'))
                                    if errors:
                                        return jsonify({'code': 400,
                                                        'description': f"Error, check our 'data': {errors}"})
                                    else:
                                        return jsonify({'code': 200,
                                                        'description': 'completed'})
                                else:
                                    return jsonify({'code': 300,
                                                    'description': 'canceled'})
                            else:
                                return jsonify({'code': 452, 'description': "KeyError: don't have key 'id'"})
                        else:
                            return jsonify({'code': 452, 'description': "KeyError: don't have 'url' key"})
                    else:
                        return jsonify({'code': 452, 'description': "KeyError: don't have 'category' key"})
                else:
                    return jsonify({'code': 452, 'description': "KeyError: don't have key 'data'"})
            else:
                return jsonify({'code': 452, 'description': "KeyError: don't have key 'type'"})


@app.route('/api/v1/post_message/<token>', methods=['POST'])
def post_message(token):
    """:param token - auth token in url

       :example {'type': str,
                'data': str,
                'category': int}"""
    print(request.json)
    if not check_token(session, token):
        return jsonify({'code': 401,
                        'description': 'invalid token'})
    else:
        if not request.json:
            return jsonify({'code': 452, 'description': "TypeError: not json request"})
        else:
            body = request.json
            if body.get('type') is not None:
                if body.get('data') is not None:
                    if body.get('category') is not None:
                        msg_type = body.get('type')
                        category = body.get('category')
                        if msg_type == 'html':
                            bot = RequestBot(session)
                            category = body.get('category')
                            data = body.get('data')
                            editor = TextEditor()
                            data = editor.html_editor(data)
                            if len(data) < 1:
                                return jsonify({'code': 400,
                                                'description': 'text is empty'})
                            errors = bot.post_message(data, category)
                            if errors:
                                return jsonify({'code': 400,
                                                'description': f"Error, check our 'data': {errors}"})
                            else:
                                return jsonify({'code': 200,
                                                'description': 'completed'})

                    else:
                        return jsonify({'code': 452, 'description': "KeyError: don't have 'category' key"})
                else:
                    return jsonify({'code': 452, 'description': "KeyError: don't have 'data' key"})

            else:
                return jsonify({'code': 452, 'description': "KeyError: don't have key 'type'"})


@app.route('/api/v1/get_stat/<token>', methods=['GET'])
def take_statistic(token):
    if not check_token(session, token):
        return jsonify({'code': 301,
                        'description': 'invalid token'})
    else:
        pass


@app.route('/api/v1/get_token/', methods=['GET'])
def get_token():
    token = session.query(Token).first()
    return jsonify({'code': 200,
                    'description': f'<token: {token.token}>'})


@app.route('/', methods=['GET'])
def check_work():
    return jsonify({'code': 200, 'description': 'server is working'})


def start_app():
    app.run(debug=True)


if __name__ == '__main__':
    app.run(debug=True)

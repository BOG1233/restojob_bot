from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from src.config import db_name
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import sessionmaker
from time import time
from secrets import token_hex

engine = create_engine(db_name, echo=False, pool_recycle=7200)
Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    chat_id = Column(Integer, primary_key=True)
    categories = Column(postgresql.ARRAY(Integer), default=[])
    status = Column(Integer, default=0)
    role = Column(Integer, default=0)

    def __init__(self, chat_id, categories=None, status=0, role=0):
        if categories is not None:
            self.chat_id = chat_id
            self.categories = categories
            self.status = 0
            self.role = 0
        else:
            self.chat_id = chat_id
            self.status = 0
            self.role = 0

    def __repr__(self):
        return f"<User {self.chat_id}>"

    def add_category(self, new_category):
        new_list = []
        new_list += self.categories
        new_list.append(int(new_category))
        self.categories = new_list

    def delete_category(self, category_id):
        new_list = []
        new_list += self.categories
        i = new_list.index(category_id)
        new_list.pop(i)
        self.categories = new_list


class Category(Base):
    __tablename__ = 'categories'
    category_id = Column(Integer, primary_key=True)
    name = Column(String(64), nullable=False)
    emoticon = Column(String(10))

    def __init__(self, category_id, name, emoticon='⚪️'):
        self.category_id = category_id
        self.name = name
        self.emoticon = emoticon

    def __repr__(self):
        return f"<User {self.category_id}, {self.name}>"


class Token(Base):
    __tablename__ = 'tokens'
    token = Column(String(30), primary_key=True)

    def __init__(self, token=None):
        self.token = token

    def __repr__(self):
        return f"<Token {self.token}>"

    def generate_token(self):
        self.token = token_hex(15)


class SentMessage(Base):
    __tablename__ = 'sent_messages'
    id = Column(Integer, primary_key=True, autoincrement=True)
    date_publish = Column(Integer)
    message_text = Column(String(2048))
    category = Column(Integer)

    def __init__(self, message_text, category):
        self.message_text = message_text
        self.date_publish = int(time())
        self.category = category

    def __repr__(self):
        return f"<Message Date: {self.date_publish}>"


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)


if __name__ == '__main__':
    pass  # 7c553c7c6fb42a157adea9df95fa81

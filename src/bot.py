import telebot
from telebot import types
from src.config import token
from src.DataBase import *
from time import sleep


def get_chat_ids(users, vacancy_id):
    chat_ids = []
    for user in users:
        if vacancy_id in user.categories:
            chat_ids.append(user.chat_id)
    return chat_ids


class RequestBot:

    def __init__(self, session):
        """
        :param session - session with database

        This class for performing get, post requests in app"""

        self.telegram = telebot.TeleBot(token)
        self.session = session

    def post_vacancy(self, data, category, vacancy_url, phone=None, parse_mode='html'):
        """ :param data - text whose will be sent users. If parse mode is html, data - html text

            :param vacancy_url - url for inline button

            :param phone - pass

            :param category - group of people whose will get this message

            :param parse_mode - optional param? if not 'html' message_text has to make"""

        users = self.session.query(User).all()
        try:
            for chat_id in get_chat_ids(users, category):
                markup = types.InlineKeyboardMarkup(row_width=1)
                markup.add(types.InlineKeyboardButton('Перейти', url=vacancy_url))
                if parse_mode == 'html':
                    self.telegram.send_message(chat_id, data, parse_mode='html', reply_markup=markup)
                sleep(0.04)
            return False
        except Exception as error:
            return error

    def post_message(self, data, category, parse_mode='html'):
        """ :param data - text whose will be sent users. If parse mode is html, data - html text

            :param category - group of people whose will get this message

            :param parse_mode - optional param? if not 'html' message_text has to make"""
        try:
            users = self.session.query(User).all()

            for chat_id in get_chat_ids(users, category):
                if parse_mode == 'html':
                    self.telegram.send_message(chat_id, data, parse_mode='html')
                sleep(0.04)
            return False
        except Exception as error:
            return error


if __name__ == '__main__':
    pass

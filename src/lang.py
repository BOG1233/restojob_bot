

ru = {
    'welcome': 'Приветствую тебя в ...',
    'welcome_user': 'Привет {}, Я могу помочь тебе найти работу',
    'main_page_user': 'Что вы хотите сделать?',
    'send_password': 'Введите пароль(токен)',
    'failed_password': 'Неверный токен или пароль',
    'change_specialization_set': 'Выбрать категории',
    'my_specialization_set': 'Мои категории',
    'show_user_specialization_set': 'Вот список ваших категорий',
    'no_user_specialization_set': 'На данны момент вы не выбрали ни одной категории',
    'delete_specialization_set': 'Удалить категорию из вашего списка',
    'choose_specializations': 'Хорошо, выберите категори:',
    'delete_specializations': 'Какие категории вы хотите убрать?',
    'delete_some_category': 'Категория удалена',
    'choose_some_category': 'Выбрана категория {}',
    'choose_alert': 'Данная категория уже выбрана',
    'main_page_admin': 'Вы находитесть в админке, что вы хотите сделать?',
    'ask_after_sending': 'Точно хотите отправить это сообщение?',
    'complete_sending': 'Начал отправку сообщений, это может занять какое то время',
    'generate_new_token': 'НОвый токен сгенерирован: {}',
    'send_admin_message': 'Отправить сообщение группе пользователей',
    'get_stat': 'Получить статистику',
    'write_message': 'Теперь отправьте текст сообщения',

}



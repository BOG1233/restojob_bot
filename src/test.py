import requests

url = 'http://127.0.0.1:5000/api/v1/'
token = '0400a93a0c527da36b286ebad5d17f'
vacancy_body = {'type': 'html',
                'data': 'Тест отправки вакансии через сервер',
                'url': 'https://restojob.ru/employer/vacancy/vacancy-view/123456/',
                'category': 1,
                'phone': '+79991324565',
                'id': 123456}
message_body = {'type': 'html',
                'data': 'Тест отправки сообщений через сервер',
                'category': 2
                }


def send_vacancy():
    rez = requests.post(url + 'post_vacancy/' + token, json=vacancy_body)
    print(rez.text)


def send_message():
    rez = requests.post(url + 'post_message/' + token, json=message_body)
    print(rez.text)


if __name__ == '__main__':
    send_vacancy()
    send_message()

import secrets
from src.DataBase import Token, Session


def create_token():
    session = Session()
    new_token = Token()
    new_token.generate_token()
    session.add(new_token)
    session.commit()
    session.close()


def check_token(session, token):
    query_tokens = session.query(Token).all()
    tokens = []
    for query_token in query_tokens:
        tokens.append(query_token.token)
    if token in tokens:
        return True
    else:
        return False

from src.config import token, devs_list
import telebot
from telebot import types
from src.lang import ru
from src.DataBase import *
from src.token_gen import *
from time import sleep


bot = telebot.TeleBot(token)

def create_main_user_interface():
    user_interface = types.InlineKeyboardMarkup(row_width=2)
    print(type(str(ru.get('my_specialization_set'))))
    user_interface.add(types.InlineKeyboardButton(ru.get('change_specialization_set'),
                                                  callback_data='change_specialization_set'),
                       types.InlineKeyboardButton(ru.get('my_specialization_set'),
                                                  callback_data='my_specialization_set'))

    user_interface.add(types.InlineKeyboardButton(ru.get('delete_specialization_set'),
                                                  callback_data='delete_specialization_set'))
    return user_interface


def create_main_admin_interface():
    admin_interface = types.InlineKeyboardMarkup()
    admin_interface.add(types.InlineKeyboardButton(ru.get('send_admin_message'), callback_data='send_admin_message'))
    admin_interface.add(types.InlineKeyboardButton(ru.get('get_stat'), callback_data='get_stat'))
    return admin_interface


def add_category(session, category_id, chat_id):
    user = session.query(User).filter_by(chat_id=chat_id).first()
    if category_id not in user.categories:
        user.add_category(category_id)
        print(user.categories)
        session.add(user)
        return True
    return False


def delete_category(session, category_id, chat_id):
    user = session.query(User).filter_by(chat_id=chat_id).first()
    if category_id in user.categories:
        user.delete_category(category_id)
        session.add(user)
        return True
    return False


def post_message(session, data, vacancy_id):
    users = session.query(User).all()
    print(get_chat_ids(users, vacancy_id))
    for chat_id in get_chat_ids(users, vacancy_id):
        bot.send_message(chat_id, data, parse_mode='html')
        sleep(0.01)
    return 0


def get_chat_ids(users, vacancy_id):
    chat_ids = []
    for user in users:
        if vacancy_id in user.categories:
            chat_ids.append(user.chat_id)
    return chat_ids


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    session = Session()
    bot.send_message(message.chat.id, ru.get('welcome'))
    list_id = [chat_id[0] for chat_id in session.query(User.chat_id).all()]
    if message.chat.id not in list_id:
        new_user = User(message.chat.id)
        session.add(new_user)
        session.commit()
        session.close()
    else:
        new_user = session.query(User).filter_by(chat_id=message.chat.id).first()
        new_user.status = 0
        session.add(new_user)
        session.commit()
        session.close()
    user_interface = create_main_user_interface()

    bot.send_message(text=ru.get('main_page_user'), chat_id=message.chat.id, reply_markup=user_interface)


@bot.message_handler(commands=['create_token', 'show_token'])
def change_token(message):
    session = Session()
    print(message)
    user = session.query(User).filter_by(chat_id=message.chat.id).first()
    if user.role == 1 or user.chat_id in devs_list:
        if message.text == '/create_token':
            new_token = Token()
            new_token.generate_token()
            session.add(new_token)
            bot.send_message(message.chat.id, ru.get('generate_new_token').format(new_token))
        elif message.text == '/show_token':
            tokens = session.query(Token).all()
            text = ''
            i = 1
            for iter_token in tokens:
                text += f'{i} token: {iter_token}\n'
            bot.send_message(message.chat.id, text)
    session.commit()
    session.close()


@bot.message_handler(func=lambda message: True)
def main_logic(message):
    session = Session()
    status = session.query(User.status).filter_by(chat_id=message.chat.id)[0][0]
    if status == 0:
        if check_token(session, message.text):
            user = session.query(User).filter_by(chat_id=message.chat.id).first()
            user.role = 1
            admin_interface = create_main_admin_interface()

            bot.send_message(message.chat.id, ru.get('main_page_admin'),
                             reply_markup=admin_interface, parse_mode='html')

        else:
            check_user = session.query(User).filter_by(chat_id=message.chat.id).first()
            check_user.status = 1
            check_user.role = 0

            user_interface = create_main_user_interface()

            bot.send_message(text=ru.get('main_page_user'), chat_id=message.chat.id, reply_markup=user_interface)

    if status == 1:
        role = session.query(User.role).filter_by(chat_id=message.chat.id)[0][0]

        if role == 0:
            if check_token(session, message.text):
                user = session.query(User).filter_by(chat_id=message.chat.id).first()
                user.role = 1
                admin_interface = create_main_admin_interface()

                bot.send_message(message.chat.id, ru.get('main_page_admin'),
                                 reply_markup=admin_interface, parse_mode='html')
            else:
                user_interface = create_main_user_interface()
                bot.send_message(message.chat.id, ru.get('main_page_user'), reply_markup=user_interface)

        elif role == 1:
            admin_interface = create_main_admin_interface()
            bot.send_message(message.chat.id, ru.get('main_page_admin'), reply_markup=admin_interface)

    if status == 2:
        role = session.query(User.role).filter_by(chat_id=message.chat.id).first()[0]
        if role == 0:
            pass

        elif role == 1:
            markup = types.InlineKeyboardMarkup(row_width=2)
            markup.add(types.InlineKeyboardButton(text='Да', callback_data='yes_send_message'),
                       types.InlineKeyboardButton(text='Нет', callback_data='no_send_message'))
            message_text = ru.get('ask_after_sending') + '\n@@@\n' + message.text + '\n@@@\n'
            last_row = session.query(SentMessage).all()[-1]
            last_row.message_text = message.text
            session.add(last_row)
            bot.send_message(message.chat.id, message_text, reply_markup=markup)

    session.commit()
    session.close()


@bot.callback_query_handler(func=lambda c: True)
def inline(c):
    session = Session()
    if c.data == 'main_user_interface':
        bot.edit_message_text(text=ru.get('main_page_user'), chat_id=c.message.chat.id,
                              message_id=c.message.message_id, reply_markup=create_main_user_interface())

    elif c.data == 'main_admin_interface':
        bot.edit_message_text(text=ru.get('main_page_admin'), chat_id=c.message.chat.id,
                              message_id=c.message.message_id, reply_markup=create_main_admin_interface())

    elif c.data == 'change_specialization_set':
        message_text = ru.get('choose_specializations') + '\n'
        markup = types.InlineKeyboardMarkup(row_width=2)
        button_set = []
        for category in session.query(Category).all():
            button_set.append(types.InlineKeyboardButton(text=category.emoticon + ' ' + category.name,
                                                         callback_data=f'category-{category.category_id}'))
        other = types.InlineKeyboardButton(text='Другое', callback_data='other_categories')
        markup.add(button_set[0], button_set[1])
        markup.add(button_set[2], other)
        print(message_text)
        markup.add(types.InlineKeyboardButton(text='Готово', callback_data='main_user_interface'))
        bot.edit_message_text(message_text, c.message.chat.id, c.message.message_id,
                              parse_mode='html', reply_markup=markup)

    elif c.data == 'other_categories':
        message_text = ru.get('choose_specializations') + '\n'
        markup = types.InlineKeyboardMarkup(row_width=2)
        button_set = []
        for category in session.query(Category).all():
            button_set.append(types.InlineKeyboardButton(text=category.emoticon + ' ' + category.name,
                                                         callback_data=f'category-{category.category_id}'))
        markup.add(button_set[3], button_set[4])
        markup.add(types.InlineKeyboardButton(text='Назад', callback_data='change_specialization_set'))
        bot.edit_message_text(message_text, c.message.chat.id, c.message.message_id,
                              parse_mode='html', reply_markup=markup)

    elif c.data == 'delete_specialization_set':
        message_text = ru.get('delete_specializations') + '\n'
        markup = types.InlineKeyboardMarkup(row_width=2)
        for category_id in session.query(User.categories).filter_by(chat_id=c.message.chat.id).first()[0]:
            category = session.query(Category).filter_by(category_id=category_id).first()
            markup.add(types.InlineKeyboardButton(text=category.emoticon + ' ' + category.name,
                                                  callback_data=f'delete_category-{category.category_id}'))

        markup.add(types.InlineKeyboardButton(text='Готово', callback_data='main_user_interface'))
        bot.edit_message_text(message_text, c.message.chat.id, c.message.message_id,
                              parse_mode='html', reply_markup=markup)

    elif 'delete_category-' in c.data:
        category_id = int(c.data.split('-')[1])
        is_delete = delete_category(session, category_id, c.message.chat.id)

        if is_delete:
            bot.answer_callback_query(callback_query_id=c.id, show_alert=False, text=ru.get('delete_some_category'))
            session.commit()
            message_text = ru.get('delete_specializations') + '\n'
            markup = types.InlineKeyboardMarkup(row_width=1)
            for category_id in session.query(User.categories).filter_by(chat_id=c.message.chat.id).first()[0]:
                category = session.query(Category).filter_by(category_id=category_id).first()
                markup.add(types.InlineKeyboardButton(text=category.emoticon + ' ' + category.name,
                                                      callback_data=f'delete_category-{category.category_id}'))
            markup.add(types.InlineKeyboardButton(text='Готово', callback_data='main_user_interface'))
            bot.edit_message_text(message_text, c.message.chat.id, c.message.message_id,
                                  parse_mode='html', reply_markup=markup)

    elif 'category-' in c.data:
        category_id = int(c.data.split('-')[1])
        is_added = add_category(session, category_id, c.message.chat.id)
        if is_added:
            bot.answer_callback_query(callback_query_id=c.id, show_alert=False,
                                      text=ru.get('choose_some_category').format(session.query(Category.name).
                                                                                 filter_by(category_id=category_id)[0][
                                                                                     0]))

        else:
            bot.answer_callback_query(callback_query_id=c.id, show_alert=False,
                                      text=ru.get('choose_alert'))
        sleep(2)

    elif c.data == 'my_specialization_set':
        message_text = ru.get('show_user_specialization_set') + '\n'
        spec_set = session.query(User.categories).filter_by(chat_id=c.message.chat.id).first()[0]
        for specialization in spec_set:
            category = session.query(Category).filter_by(category_id=specialization).first()
            message_text += category.emoticon + ' ' + category.name + '\n'
        markup = types.InlineKeyboardMarkup(row_width=1)
        markup.add(types.InlineKeyboardButton(text='Назад', callback_data='main_user_interface'))
        bot.edit_message_text(text=message_text, chat_id=c.message.chat.id,
                              message_id=c.message.message_id, parse_mode='html', reply_markup=markup)

    elif c.data == 'send_admin_message':
        message_text = ru.get('choose_specializations') + '\n'
        markup = types.InlineKeyboardMarkup(row_width=1)
        for category in session.query(Category).all():
            markup.add(types.InlineKeyboardButton(text=category.emoticon + ' ' + category.name,
                                                  callback_data=f'send_message-{category.category_id}'))
        markup.add(types.InlineKeyboardButton(text='Назад', callback_data='main_admin_interface'))
        bot.edit_message_text(message_text, c.message.chat.id, c.message.message_id,
                              parse_mode='html', reply_markup=markup)

    elif 'send_message-' in c.data:
        markup = types.InlineKeyboardMarkup(row_width=1)
        markup.add(types.InlineKeyboardButton(text='Назад', callback_data='back_send_admin_message'))
        bot.edit_message_text(text=ru.get('write_message'), chat_id=c.message.chat.id, message_id=c.message.message_id,
                              parse_mode='html', reply_markup=markup)
        admin = session.query(User).filter_by(chat_id=c.message.chat.id).first()
        new_message = SentMessage(' ', int(c.data.split('-')[1]))
        session.add(new_message)
        admin.status = 2

    elif c.data == 'yes_send_message':
        last_row = session.query(SentMessage).all()[-1]
        bot.edit_message_text(text=ru.get('complete_sending') + '\n' + ru.get('main_page_admin'),
                              chat_id=c.message.chat.id, message_id=c.message.message_id,
                              reply_markup=create_main_admin_interface())
        post_message(session, last_row.message_text, last_row.category)

    elif c.data == 'no_send_message':
        admin = session.query(User).filter_by(chat_id=c.message.chat.id).first()
        admin.status = 1
        last_row = session.query(SentMessage).all()[-1]
        session.delete(last_row)
        admin_interface = create_main_admin_interface()
        bot.send_message(c.message.chat.id, ru.get('main_page_admin'), reply_markup=admin_interface)

    elif c.data == 'back_send_admin_message':
        admin = session.query(User).filter_by(chat_id=c.message.chat.id).first()
        admin.status = 1
        last_row = session.query(SentMessage).all()[-1]
        session.delete(last_row)
        message_text = ru.get('choose_specializations') + '\n'
        markup = types.InlineKeyboardMarkup(row_width=1)
        for category in session.query(Category).all():
            markup.add(types.InlineKeyboardButton(text=category.emoticon + ' ' + category.name,
                                                  callback_data=f'send_message-{category.category_id}'))
        markup.add(types.InlineKeyboardButton(text='Назад', callback_data='main_admin_interface'))
        bot.edit_message_text(message_text, c.message.chat.id, c.message.message_id,
                              parse_mode='html', reply_markup=markup)

    elif c.data == 'get_stat':
        pass
    session.commit()
    session.close()


def start_bot():
    print('start')
    bot.polling(none_stop=True)
